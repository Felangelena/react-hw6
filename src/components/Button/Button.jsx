import PropTypes from "prop-types";
import './Button.scss';

const Button = ({backgroundColor, text, handleClick, className}) => {

    return (
        <button type="button" className={className}
        style={{backgroundColor: backgroundColor}}
        onClick={handleClick}
        data-testid={'button'}>
        {text}
        </button>
    )
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
    handleClick: PropTypes.func.isRequired
}

Button.defaultProps = {
    text: "Click me!",
    backgroundColor: "black",
    className: "btn",
    handleClick: () => {console.log("Clicked")}
}

export default Button;