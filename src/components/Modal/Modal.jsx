import PropTypes from "prop-types";
import './Modal.scss';

const Modal = ({header, text, actions, close, isCloseButton}) => {

  return (
      <div className="overlay" onClick={close} data-testid={'overlay'}>
        <div className="modal-win" onClick={(e) => e.stopPropagation()} data-testid={'modal'}>
          <div className="modal-win__header">
            <h2>{header}</h2>
            {isCloseButton && <div className="btn-close" onClick={close} data-testid={'close-button'}></div>}
          </div>
          <div className="modal-win__text">
            <p>{text}</p>
          </div>
          <div className="modal-win__btns">
            {actions}
          </div>
        </div>
      </div>
  )
}

Modal.propTypes = {
  actions: PropTypes.array.isRequired,
}

Modal.defaultProps = {
  header: "Add to cart",
  text: "Do you want to do this action?",
  actions: [<button key="1">Yes</button>,<button key="2">No</button>]
}

export default Modal;