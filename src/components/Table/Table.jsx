import React, {useMemo} from 'react'
import { useTable } from 'react-table'
import { useSelector, useDispatch } from "react-redux";
import { openModalAC, toogleFavoritesAC } from '../../store/actions';
import COLUMNS from './columns';
import { useLocation } from 'react-router-dom';
import Button from '../Button/Button.jsx';
import {ReactComponent as Star} from '../ProductListItem/icons/star.svg';
import './table.scss';

export const Table = () => {
    const dispatch = useDispatch();
    const openModal = (id) => dispatch(openModalAC(id));
    const toogleFavorites = (id) => dispatch(toogleFavoritesAC(id));

    const goods = useSelector((state) => state.data.data);
    const cart = useSelector((state) => state.data.cart);
    const favorites = useSelector((state) => state.data.favorites);
    const location = useLocation();
    let data, lastColumn;

    if (location.pathname === "/"){
        lastColumn = [{
            Header: "Add to favorite",
            Cell: tableProps => (
                <div className="btn-favorite"><Star className={favorites.includes(tableProps.row.original.id) ? "star active" : "star"} onClick={() => toogleFavorites(tableProps.row.original.id)}/></div>
            )},
            {Header: "Add to cart",
            Cell: tableProps => (
                <Button className="btn-table" backgroundColor="black" text="Add to cart" handleClick={() => openModal(tableProps.row.original.id)}/>
            )}
        ];
        data = goods;
    }

    if (location.pathname === "/favorites"){
        lastColumn = [{
            Header: "Add to favorite",
            Cell: tableProps => (
                <div className="btn-favorite"><Star className={favorites.includes(tableProps.row.original.id) ? "star active" : "star"} onClick={() => toogleFavorites(tableProps.row.original.id)}/></div>
            )},
            {Header: "Add to cart",
            Cell: tableProps => (
                <Button className="btn-table" backgroundColor="black" text="Add to cart" handleClick={() => openModal(tableProps.row.original.id)}/>
            )}
        ];
        data = goods.filter(({id}) => favorites.includes(id));
    }

    if (location.pathname === "/cart"){
        lastColumn = [{
            Header: "Delete",
            Cell: tableProps => (
                <div className="card__delete" onClick={() => openModal(tableProps.row.original.id)}>X</div>
            )
        }];
        data = goods.filter(({id}) => cart.includes(id));
    }


    const columns = useMemo(() => [...COLUMNS, ...lastColumn], [])

    const tableInstance = useTable({
        columns,
        data
    })

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow
    } = tableInstance;

    return (
    <table {...getTableProps()}>
        <thead>
            {headerGroups.map((headerGroup) => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map(column => (
                    <th {...column.getHeaderProps()}>{column.render("Header")}</th>
                    ))}
                </tr>
            ))}
        </thead>
        <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
                prepareRow(row);
                return (
                    <tr {...row.getRowProps()}>
                    {row.cells.map(cell => {
                        return <td {...cell.getCellProps()}>{cell.render("Cell")}</td>;
                    })}
                    </tr>
                );
            })}
        </tbody>
    </table>
    )
}
