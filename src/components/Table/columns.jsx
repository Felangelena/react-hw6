const COLUMNS =
    [
        {
            Header: "Item",
            accessor: "title"
        },
        {
            Header: "Price",
            accessor: "price"
        },
        {
            Header: "Color",
            accessor: "color"
        },
        {
            Header: "Image",
            accessor: "image",
            Cell: tableProps => (
                <img
                    src={tableProps.row.original.image}
                    width={60}
                    alt={tableProps.row.original.title}
                />
            )
        }
    ]

export default COLUMNS;
