import { render, screen, fireEvent } from '@testing-library/react'
import renderer from 'react-test-renderer';
import Button from '../../Button/Button';

describe('Button component', () => {
    it('renders component Button', () => {
        render(<Button />);
        const button = screen.getByTestId('button');
        expect(button).toBeInTheDocument();
    });

    it('renders with the provided text', () => {
        render(<Button text="Click Me" />);
        const button = screen.getByText('Click Me');
        expect(button).toBeInTheDocument();
    });

    it('renders with default background color and class', () => {
        render(<Button text="Default Button" />);
        const button = screen.getByTestId('button');
        expect(button).toHaveStyle('backgroundColor: black');
        expect(button).toHaveClass('btn');
    });

    it('renders with custom background color and class', () => {
        render(
        <Button text="Custom Button" backgroundColor="blue" className="custom" />
        );
        const button = screen.getByTestId('button');
        expect(button).toHaveStyle('backgroundColor: blue');
        expect(button).toHaveClass('custom');
    });

    it('calls the provided handleClick function when clicked', () => {
        const mockClick = jest.fn();
        render(<Button text="Click Me" handleClick={mockClick} />);
        const button = screen.getByTestId('button');

        fireEvent.click(button);

        expect(mockClick).toHaveBeenCalledTimes(1);
    });

    it("should match snapshot", () => {
        const button = renderer.create(<Button />);
        let tree = button.toJSON();
        expect(tree).toMatchSnapshot();
    });

    it("Add to cart should match snapshot", () => {
        const { asFragment } = render(<Button className="card__btn" backgroundColor="black" text="Add to cart"/>);
        expect(asFragment(<Button/>)).toMatchSnapshot();
    });

    it("Delete should match snapshot", () => {
        const { asFragment } = render(<Button className="card__btn" backgroundColor="black" text="Delete"/>);
        expect(asFragment(<Button/>)).toMatchSnapshot();
    });

    it("Ok should match snapshot", () => {
        const { asFragment } = render(<Button className='modal__btn' backgroundColor="maroon" text="Ok"/>);
        expect(asFragment(<Button/>)).toMatchSnapshot();
    });

    it("Cancel should match snapshot", () => {
        const { asFragment } = render(<Button className='modal__btn' backgroundColor="maroon" text="Cancel"/>);
        expect(asFragment(<Button/>)).toMatchSnapshot();
    });
});