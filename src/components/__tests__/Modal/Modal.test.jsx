import { render, screen, fireEvent } from '@testing-library/react'
import renderer from 'react-test-renderer';
import Modal from '../../Modal/Modal';

describe('Modal component', () => {
    it('renders with default header and text', () => {
        render(<Modal actions={[]} />);
        const header = screen.getByText('Add to cart');
        const text = screen.getByText('Do you want to do this action?');
        expect(header).toBeInTheDocument();
        expect(text).toBeInTheDocument();
    });

    it('renders with custom header and text', () => {
        render(<Modal header="Custom Header" text="Custom Text" actions={[]} />);
        const header = screen.getByText('Custom Header');
        const text = screen.getByText('Custom Text');
        expect(header).toBeInTheDocument();
        expect(text).toBeInTheDocument();
    });

    it('renders actions provided as children', () => {
        const actions = [<button key="1">Yes</button>, <button key="2">No</button>];
        render(<Modal actions={actions} />);
        const yesButton = screen.getByText('Yes');
        const noButton = screen.getByText('No');
        expect(yesButton).toBeInTheDocument();
        expect(noButton).toBeInTheDocument();
    });

    it('calls the close function when overlay is clicked', () => {
        const mockClose = jest.fn();
        render(<Modal actions={[]} close={mockClose} />);
        const overlay = screen.getByTestId('overlay');// Clicking the overlay should trigger close
        fireEvent.click(overlay);
        expect(mockClose).toHaveBeenCalledTimes(1);
    });

    it('does not call the close function when modal content is clicked', () => {
        const mockClose = jest.fn();
        render(<Modal actions={[]} close={mockClose} />);
        const modal = screen.getByTestId('modal');
        fireEvent.click(modal);
        expect(mockClose).not.toHaveBeenCalled();
    });

    it('calls the close function when close button is clicked', () => {
        const mockClose = jest.fn();
        render(<Modal actions={[]} close={mockClose} isCloseButton={true} />);
        const closeButton = screen.getByTestId('close-button');
        fireEvent.click(closeButton);
        expect(mockClose).toHaveBeenCalledTimes(1);
    });

    it('does not render close button when isCloseButton is false', () => {
        render(<Modal actions={[]} isCloseButton={false} />);
        const closeButton = screen.queryByTestId('close-button');
        expect(closeButton).toBeNull();
    });

    it("should match snapshot", () => {
        const button = renderer.create(<Modal/>);
        let tree = button.toJSON();
        expect(tree).toMatchSnapshot();
    });

    it("Add to cart should match snapshot", () => {
        const { asFragment } = render(<Modal header="Add item" isCloseButton={true} text="Do you want add item to the cart?"/>);
        expect(asFragment(<Modal/>)).toMatchSnapshot();
    });

    it("Delete should match snapshot", () => {
        const { asFragment } = render(<Modal header="Delete item?" isCloseButton={true} text="Do you want to delete this item from the cart?"/>);
        expect(asFragment(<Modal/>)).toMatchSnapshot();
    });
});