import dataReducer from '../../../store/data.reducer';

import {
    FETCH_DATA,
    OPEN_MODAL,
    CLOSE_MODAL,
    ADD_TO_CART,
    DELETE_FROM_CART,
    TOGGLE_FAVORITES,
    CLEAR_CART,
} from "../../../store/actions.js";

describe('dataReducer', () => {
    const initialState = {
        openModal: false,
        data: [],
        id: 0,
        cart: [],
        favorites: []};

    it('handles FETCH_DATA action', () => {
        const action = {
        type: FETCH_DATA,
        payload: [{ id: 1, name: 'Item 1' }],
        };

        const newState = dataReducer(initialState, action);

        expect(newState).toEqual({
        ...initialState,
        data: [...initialState.data, ...action.payload],
        });
    });

    it('handles OPEN_MODAL action', () => {
        const action = {
        type: OPEN_MODAL,
        payload: 123,
        };

        const newState = dataReducer(initialState, action);

        expect(newState).toEqual({
        ...initialState,
        openModal: true,
        id: action.payload,
        });
    });

    it('handles CLOSE_MODAL action', () => {
        const action = {
        type: CLOSE_MODAL,
        };

        const newState = dataReducer(initialState, action);

        expect(newState).toEqual({
        ...initialState,
        openModal: false,
        });
    });

    it('handles ADD_TO_CART action for adding', () => {
        const action = {
            type: ADD_TO_CART,
            payload: 123,
        };

        const newState = dataReducer(initialState, action);

        expect(newState).toEqual({
            ...initialState,
            cart: [...initialState.cart, action.payload],
            openModal: false
        });
    });

    it('handles DELETE_FROM_CART action for removing', () => {
        const initialStateCart = {
            ...initialState,
            cart: [123, 456],
        };

        const action = {
            type: DELETE_FROM_CART,
            payload: 123,
        };

        const newState = dataReducer(initialStateCart, action);

        expect(newState).toEqual({
            ...initialStateCart,
            cart: initialStateCart.cart.filter((item) => item !== action.payload)
        });
    });

    it('handles TOGGLE_FAVORITES action for adding', () => {
        const action = {
            type: TOGGLE_FAVORITES,
            payload: 123,
        };

        const newState = dataReducer(initialState, action);

        expect(newState).toEqual({
            ...initialState,
            favorites: [...initialState.cart, action.payload],
        });
    });

    it('handles TOGGLE_FAVORITES action for removing', () => {
        const initialStateFavorites = {
            ...initialState,
            favorites: [123, 456],
        };

        const action = {
            type: TOGGLE_FAVORITES,
            payload: 456,
        };

        const newState = dataReducer(initialStateFavorites, action);

        expect(newState).toEqual({
            ...initialStateFavorites,
            favorites: initialStateFavorites.favorites.filter((item) => item !== action.payload)
        });
    });

    it('handles CLEAR_CART action', () => {
        const action = {
        type: CLEAR_CART,
        };

        const newState = dataReducer(initialState, action);

        expect(newState).toEqual({
        ...initialState,
        cart: [],
        });
    });

    it('returns the current state for an unknown action', () => {
        const action = {
        type: 'UNKNOWN_ACTION',
        };

        const newState = dataReducer(initialState, action);

        expect(newState).toEqual(initialState);
    });
});